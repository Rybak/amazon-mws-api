<?php

namespace WebPapers\Amazon\Orders\Response;

use WebPapers\Amazon\Orders\Result\ListOrdersResult;

class ListOrdersByNextTokenResponse
{
    /**
     * @var ListOrdersResult
     */
    public $ListOrdersByNextTokenResult;

    /**
     * @return ListOrdersResult
     */
    public function getResult()
    {
        return $this->ListOrdersByNextTokenResult;
    }
}