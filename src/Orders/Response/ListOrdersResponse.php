<?php

namespace WebPapers\Amazon\Orders\Response;

use WebPapers\Amazon\Orders\Result\ListOrdersResult;

class ListOrdersResponse
{
    /**
     * @var ListOrdersResult
     */
    public $ListOrdersResult;

    /**
     * @return ListOrdersResult
     */
    public function getResult()
    {
        return $this->ListOrdersResult;
    }
}