<?php

namespace WebPapers\Amazon\Orders\Serializer;

use Sabre\Xml\Reader;
use Sabre\Xml\XmlDeserializable;

class DateTimeXmlDeserializer implements XmlDeserializable
{
    /**
     * @param Reader $reader
     *
     * @return \DateTime|mixed|null
     * @throws \Exception
     */
    static function xmlDeserialize(Reader $reader)
    {
        try {
            $value = $reader->parseInnerTree();
        } catch (\Exception $exception) {
            return null;
        }

        return new \DateTime($value);
    }
}