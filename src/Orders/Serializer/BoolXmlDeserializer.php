<?php

namespace WebPapers\Amazon\Orders\Serializer;

use Sabre\Xml\Reader;
use Sabre\Xml\XmlDeserializable;

class BoolXmlDeserializer implements XmlDeserializable
{
    /**
     * @param Reader $reader
     *
     * @return bool
     */
    static function xmlDeserialize(Reader $reader): bool
    {
        try {
            $value = $reader->parseInnerTree() === 'true';
        } catch (\Exception $e) {
            return false;
        }

        return $value;
    }
}