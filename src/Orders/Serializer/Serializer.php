<?php

namespace WebPapers\Amazon\Orders\Serializer;

use UnexpectedValueException;
use WebPapers\Amazon\Common\RequestInterface;
use WebPapers\Amazon\Common\Serializer\Serializer as BaseSerializer;
use WebPapers\Amazon\Orders\Request\ListOrdersByNextTokenRequest;
use WebPapers\Amazon\Orders\Request\ListOrdersRequest;

class Serializer extends BaseSerializer
{
    const DATE_FORMAT = \DateTime::ATOM;

    private $xmlDeserializer;

    public function __construct()
    {
        $this->xmlDeserializer = new XmlDeserializer();
    }

    public function serialize(RequestInterface $request)
    {
        switch (true) {
            case $request instanceof ListOrdersRequest:
                $action = 'ListOrders';
                break;

            case $request instanceof ListOrdersByNextTokenRequest:
                $action = 'ListOrdersByNextToken';
                break;

            default:
                throw new UnexpectedValueException(get_class($request) . ' is not supported.');
        }

        return $this->serializeProperties($action, $request);
    }

    public function unserialize($response)
    {
        return $this->xmlDeserializer->parse($response);
    }
}