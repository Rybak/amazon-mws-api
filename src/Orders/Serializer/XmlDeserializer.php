<?php

namespace WebPapers\Amazon\Orders\Serializer;

use WebPapers\Amazon\Common\Serializer\XmlDeserializer as BaseXmlDeserializer;
use WebPapers\Amazon\Orders\Entity\Amount;
use WebPapers\Amazon\Orders\Entity\Order;
use WebPapers\Amazon\Orders\Entity\ShippingAddress;
use WebPapers\Amazon\Orders\Response\ListOrdersByNextTokenResponse;
use WebPapers\Amazon\Orders\Response\ListOrdersResponse;
use WebPapers\Amazon\Orders\Result\ListOrdersResult;

class XmlDeserializer extends BaseXmlDeserializer
{
    const NS = 'https://mws.amazonservices.com/Orders/2013-09-01';

    public function getElementMap()
    {
        $ns = sprintf('{%s}', static::NS);

        return [
            "{$ns}ListOrdersResponse" => $this->mapObject(ListOrdersResponse::class),
            "{$ns}ListOrdersByNextTokenResponse" => $this->mapObject(ListOrdersByNextTokenResponse::class),
            "{$ns}ListOrdersByNextTokenResult" => $this->mapObject(ListOrdersResult::class),
            "{$ns}ListOrdersResult"   => $this->mapObject(ListOrdersResult::class),
            "{$ns}Orders"             => $this->mapCollection("{$ns}Order", Order::class),
            "{$ns}OrderTotal"         => $this->mapObject(Amount::class),
            "{$ns}ShippingAddress"    => $this->mapObject(ShippingAddress::class),
            "{$ns}LatestShipDate"     => DateTimeXmlDeserializer::class,
            "{$ns}PurchaseDate"       => DateTimeXmlDeserializer::class,
            "{$ns}LastUpdateDate"     => DateTimeXmlDeserializer::class,
            "{$ns}EarliestShipDate"   => DateTimeXmlDeserializer::class,
            "{$ns}IsReplacementOrder" => BoolXmlDeserializer::class,
            "{$ns}IsBusinessOrder"    => BoolXmlDeserializer::class,
            "{$ns}IsPremiumOrder"     => BoolXmlDeserializer::class,
            "{$ns}IsPrime"            => BoolXmlDeserializer::class,
        ];
    }
}