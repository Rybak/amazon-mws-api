<?php

namespace WebPapers\Amazon\Orders\Result;

class ListOrdersResult
{
    /**
     * @var array
     */
    public $Orders;

    /**
     * @var string
     */
    public $NextToken;

    /**
     * ListOrdersResult constructor.
     */
    public function __construct()
    {
        $this->Orders = [];
    }

    /**
     * @return array
     */
    public function getOrders()
    {
        return $this->Orders;
    }

    /**
     * @return string
     */
    public function getNextToken()
    {
        return $this->NextToken;
    }
}