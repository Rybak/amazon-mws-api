<?php

namespace WebPapers\Amazon\Orders\Entity;

use WebPapers\Amazon\Common\Serializer\MetadataInterface;

class ShippingAddress implements MetadataInterface
{
    /**
     * @var string
     */
    public $Name;

    /**
     * @var string
     */
    public $City;

    /**
     * @var string
     */
    public $PostalCode;

    /**
     * @var string
     */
    public $StateOrRegion;

    /**
     * @var string
     */
    public $CountryCode;

    /**
     * @var string
     */
    public $AddressLine1;

    /**
     * @return array
     */
    public function getMetadata()
    {
        return [
            'Name' => ['type' => 'scalar'],
            'City' => ['type' => 'scalar'],
            'PostalCode' => ['type' => 'scalar'],
            'StateOrRegion' => ['type' => 'scalar'],
            'CountryCode' => ['type' => 'scalar'],
            'AddressLine1' => ['type' => 'scalar'],
        ];
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return implode(PHP_EOL, [
            $this->Name,
            $this->City,
            $this->CountryCode,
            $this->StateOrRegion,
            $this->PostalCode,
            $this->AddressLine1,
        ]);
    }
}