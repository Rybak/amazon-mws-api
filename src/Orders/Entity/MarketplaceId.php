<?php

namespace WebPapers\Amazon\Orders\Entity;

class MarketplaceId
{
    /**
     * @var string
     */
    public $Id;

    public function getMetaData()
    {
        return [
            'Id' => ['type' => 'scalar']
        ];
    }
}