<?php

namespace WebPapers\Amazon\Orders\Entity;

use WebPapers\Amazon\Common\Serializer\MetadataInterface;

class FulfillmentChannel implements MetadataInterface
{
    /**
     * @var string
     */
    public $Channel;

    /**
     * @return array
     */
    function getMetadata(): array
    {
        return [
            'Channel' => ['type' => 'scalar'],
        ];
    }
}