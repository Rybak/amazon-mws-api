<?php

namespace WebPapers\Amazon\Orders\Entity;

class Order
{
    public $OrderType;

    public $LatestShipDate;

    public $PurchaseDate;

    public $AmazonOrderId;

    public $BuyerEmail;

    public $IsReplacementOrder;

    public $LastUpdateDate;

    public $NumberOfItemsShipped;

    public $ShipServiceLevel;

    public $OrderStatus;

    public $SalesChannel;

    public $IsBusinessOrder;

    public $NumberOfItemsUnshipped;

    public $BuyerName;

    public $OrderTotal;

    public $IsPremiumOrder;

    public $EarliestShipDate;

    public $MarketplaceId;

    public $FulfillmentChannel;

    public $PaymentMethod;

    public $IsPrime;

    public $ShipmentServiceLevelCategory;

    public $ShippingAddress;

    public $SellerOrderId;
}