<?php

namespace WebPapers\Amazon\Orders\Entity;

use WebPapers\Amazon\Common\Serializer\MetadataInterface;

/**
 * The currency code and value.
 */
final class Amount implements MetadataInterface
{
    /**
     * @var string
     */
    public $CurrencyCode;

    /**
     * @var string
     */
    public $Amount;

    /**
     * {@inheritDoc}
     */
    public function getMetadata(): array
    {
        return [
            'CurrencyCode' => ['type' => 'scalar'],
            'Amount'       => ['type' => 'scalar'],
        ];
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return implode(' ', [$this->Amount, $this->CurrencyCode]);
    }
}