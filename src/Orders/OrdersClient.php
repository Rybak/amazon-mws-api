<?php

namespace WebPapers\Amazon\Orders;

use WebPapers\Amazon\Common\Client as BaseClient;
use WebPapers\Amazon\Common\Serializer\SerializerInterface;
use WebPapers\Amazon\Orders\Response\ListOrdersResponse;
use WebPapers\Amazon\Orders\Serializer\Serializer;

class OrdersClient extends BaseClient
{
    const API_SECTION = 'Orders';
    const API_VERSION = '2013-09-01';

    /**
     * OrdersClient constructor.
     *
     * @param SerializerInterface|null $serializer
     */
    public function __construct(SerializerInterface $serializer = null)
    {
        parent::__construct($serializer ?? new Serializer());
    }

    /**
     * @param $request
     *
     * @return ListOrdersResponse
     */
    public function getOrdersList($request)
    {
        return $this->send($request)->wait();
    }
}