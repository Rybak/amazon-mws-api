<?php

namespace WebPapers\Amazon\Orders\Request;

use WebPapers\Amazon\Common\RequestInterface;

class ListOrdersRequest implements RequestInterface
{
    /**
     * @var array
     */
    public $MarketplaceId;

    /**
     * @var string
     */
    public $CreatedAfter;

    /**
     * @var string
     */
    public $LastUpdatedAfter;

    /**
     * @var string
     */
    public $FulfillmentChannel;

    /**
     * ListOrdersRequest constructor.
     *
     * @param             $createdAfter
     * @param array       $marketPlaceId
     * @param string|null $fulfillmentChannel
     */
    public function __construct($createdAfter, array $marketPlaceId, string $fulfillmentChannel = null)
    {
        $this->CreatedAfter = $createdAfter;
        $this->MarketplaceId = $marketPlaceId;
        $this->FulfillmentChannel = $fulfillmentChannel;
    }

    /**
     * @return array
     */
    public function getMetadata()
    {
        return [
            'MarketplaceId'      => ['type' => 'choice', 'multiple' => true],
            'FulfillmentChannel' => ['type' => 'choice', 'multiple' => true, 'namespace' => 'Channel', 'choices' => ['AFN', 'MFN']],
            'OrderStatus'        => ['type' => 'scalar'],
            'CreatedAfter'       => ['type' => 'datetime'],
            'LastUpdatedAfter'   => ['type' => 'datetime'],
        ];
    }

    public function getMarketplaceId()
    {
        return $this->MarketplaceId;
    }

    public function setMarketplaceId(array $marketplaceIdArray)
    {
        $this->MarketplaceId = $marketplaceIdArray;

        return $this;
    }

    public function setCreatedAfter($createdAfter)
    {
        $this->CreatedAfter = $createdAfter;

        return $this;
    }

    public function setLastUpdatedAfter($lastUpdatedAfter)
    {
        $this->LastUpdatedAfter = $lastUpdatedAfter;

        return $this;
    }

    public function setFulfillmentChannel($fulfillmentChannel)
    {
        $this->FulfillmentChannel = $fulfillmentChannel;

        return $this;
    }
}