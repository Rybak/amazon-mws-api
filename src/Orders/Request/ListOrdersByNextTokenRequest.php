<?php

namespace WebPapers\Amazon\Orders\Request;

use WebPapers\Amazon\Common\RequestInterface;

class ListOrdersByNextTokenRequest implements RequestInterface
{
    /**
     * @var string
     */
    public $NextToken;

    /**
     * ListOrdersByNextTokenRequest constructor.
     *
     * @param string $nextToken
     */
    public function __construct(string $nextToken)
    {
        $this->NextToken = $nextToken;
    }

    /**
     * @return array
     */
    public function getMetadata()
    {
        return [
            'NextToken' => ['type' => 'scalar'],
        ];
    }
}